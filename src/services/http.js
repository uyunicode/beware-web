import axios from "axios";

/**
 * HTTP services to rest api
 */
export class HTTP {
    static host = 'http://ec2-3-21-164-145.us-east-2.compute.amazonaws.com';

    /**
     * method http get request
     * @param {string} url : url link
     */
    static get = async (url) => {
        try {
            return await axios.get(this.host + url);
        } catch (error) {
            console.log("Error in method HTTP get: ", error);
            return null;
        }
    }

    /**
     * method http post request
     * @param {string} url : url link
     * @param {json} body : body json to send
     */
    static post = async (url, body) => {
        try {
            return await axios.post(this.host + url, body);
        } catch (error) {
            console.log("Error in method HTTP post: ", error);
            return null;
        }
    }

    /**
     * method http put request
     * @param {string} url : url link
     * @param {json} body : body json to send
     */
    static put = async (url, body) => {
        try {
            return await axios.put(this.host + url, body);
        } catch (error) {
            console.log("Error in method HTTP put: ", error);
            return null;
        }
    }

    /**
     * method http post multipart/form-data request
     * @param {string} url : url link
     * @param {json} body : body json to send
     */
     static postFile = async (url, body) => {
        try {
            const config = {
                headers: {
                    'content-type': 'multipart/form-data'
                }
            }
            return await axios.post(this.host + url, body, config);
        } catch (error) {
            console.log("Error in method HTTP post: ", error);
            return null;
        }
    }
}