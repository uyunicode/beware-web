import io from "socket.io-client";

/**
 * WebSocket services to socket server
 */
export class SocketClient {

    constructor() {
        let data = JSON.parse(sessionStorage.getItem('USER_AUTH'));
        console.log(`data: ${data.jwt}`);
        this.socket = io(
            'http://ec2-3-21-164-145.us-east-2.compute.amazonaws.com:3000',
            {
                transports: ["websocket"], 
                query: {"token": data.jwt},
            });
    }
}