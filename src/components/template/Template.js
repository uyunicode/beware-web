import React, { Component } from 'react'

import NavBar from './NavBar';
import Footer from './Footer';
import HomePage from './HomePage';

export default class Template extends Component {
    render() {
        return (
            <>
                <NavBar />
                <HomePage content={this.props.content} pagetitle={this.props.pagetitle}></HomePage>
                <Footer />
            </>
        )
    }
}
