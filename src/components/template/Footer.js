import React, { Component } from 'react'
import { Col, Row } from 'react-bootstrap'

export default class Footer extends Component {
    render() {
        return (
            <footer style={{
                position: 'fixed',
                left: '0',
                bottom: '0',
                width: '100%'
            }} className="text-center bg-dark text-white">
                <div className="text-center p-3">
                    <Row>
                        <Col>
                            😁😁 2021 Copyright: -
                            <a className="text-white" href="https://mdbootstrap.com/">uyunicode.com</a>
                        </Col>
                        <Col></Col>
                        <Col>
                            <a href="https://storyset.com/people">People illustrations by Storyset</a>
                        </Col>
                    </Row>
                </div>
                {/* Copyright */}
            </footer>
        )
    }
}
