import React, { Component } from 'react'
import { Nav, Navbar, Container, NavDropdown } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';

export default class NavBar extends Component {
    render() {
        if (!sessionStorage.getItem('USER_AUTH')) {
            return (
                <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                    <Container>
                        <Navbar.Brand href="#home">Beware Web</Navbar.Brand>
                        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                        <Navbar.Collapse id="responsive-navbar-nav">
                            <Nav className="me-auto">

                            </Nav>
                            <Nav>
                                <Nav.Link eventKey={2} href="#memes">
                                    Ayuda
                                </Nav.Link>
                                <Nav.Link eventKey={2} href="/login">
                                    Login
                                </Nav.Link>
                            </Nav>
                        </Navbar.Collapse>
                    </Container>
                </Navbar>
            );
        } else {
            let data = JSON.parse(sessionStorage.getItem('USER_AUTH'));
            return (
                <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                    <Container>
                        <Navbar.Brand href="/">Beware Web</Navbar.Brand>
                        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                        <Navbar.Collapse id="responsive-navbar-nav">
                            <Nav className="me-auto">
                                <NavDropdown title="Gestion de Usuario" id="collasible-nav-dropdown">
                                    <NavDropdown.Item to="/police_manage">Gestionar Policia</NavDropdown.Item>
                                    <NavDropdown.Item to="/victimizer_manage">Gestionar Victimizador</NavDropdown.Item>
                                </NavDropdown>
                                <NavDropdown title="Gestion de Pedido de Ayuda" id="collasible-nav-dropdown">
                                    <NavDropdown.Item>
                                        <NavLink to="/request_management">Solicitudes de Auxilio</NavLink>
                                    </NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                                    <NavDropdown.Divider />
                                    <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
                                </NavDropdown>
                            </Nav>
                            <Nav>
                                <NavDropdown title={data.data.name} id="collasible-nav-dropdown">
                                    <NavDropdown.Item href="/login">Log Out</NavDropdown.Item>
                                </NavDropdown>
                            </Nav>
                        </Navbar.Collapse>
                    </Container>
                </Navbar>
            );
        }
    }
}
