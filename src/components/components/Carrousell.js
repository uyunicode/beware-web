import Carousel from 'react-bootstrap/Carousel';

import React, { Component } from 'react'
import { Col, Image, Nav } from 'react-bootstrap';
import StopRacismLogo from '../assets/stop-racism-amico.svg';
import MyPasswordBro from '../assets/my-password-bro.svg';
import GenderViolenceRafiki from '../assets/gender-violence-rafiki.svg';

export default class Carrousell extends Component {
    render() {
        return (
            <Carousel>
                <Carousel.Item>
                    <Col xs={6} md={4}>
                        <Image src={StopRacismLogo} />
                    </Col>
                    <Carousel.Caption>
                        <h3>Apoyo a las victimas</h3>
                        <p>contra la violencia intrafamiliar, con apoyo de la policia.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <Col xs={6} md={4}>
                        <Image src={MyPasswordBro} />
                    </Col>
                    <Carousel.Caption>
                        <h3>Medio de Pedido de Auxilio</h3>
                        <p>Software para el apoyo de las personas de confianza y la policia.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <Col xs={6} md={4}>
                        <Image src={GenderViolenceRafiki} />
                    </Col>
                    <Carousel.Caption>
                        <h3>Test de violecia intrafamiliar</h3>
                        <p>Realiza el test de la aplicación para averiguar si corres peligro <br /> a veces un@ no sabe si vive en un entorno de violencia.</p>
                        <Nav.Item>
                            <Nav.Link href="/test_intra_family_violence" className="text-light">Test de violencia familiar</Nav.Link>
                        </Nav.Item>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
        )
    }
}
