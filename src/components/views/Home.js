import React, { Component } from 'react';

import Template from '../template/Template';

export default class Home extends Component {
    render() {
        return (
            <Template pagetitle="Title Home Page" content={this.contentPage()}></Template>
        )
        // return (

        // );
    }

    contentPage() {
        return (
            <h3>Home Content</h3>
        );
    }
}
