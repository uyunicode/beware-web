import React, { Component } from 'react'
import NavBar from '../../../template/NavBar'
import Footer from '../../../template/Footer'
import { Button, Card, Form, Image, Nav, Row } from 'react-bootstrap'
import loginPana from '../../../assets/login-pana.svg'
import { HTTP } from '../../../../services/http'

export default class Login extends Component {

    state = {
        email: '',
        password: ''
    }

    onChangeEmail = (e) => {
        console.log(e.target.value);
        this.setState({ email: e.target.value });
    }

    onChangePassword = e => {
        console.log(e.target.value);
        this.setState({ password: e.target.value });
    }

    onSubmitSignIn = async event => {
        event.preventDefault();
        console.log(this.state);
        let response = await HTTP.post('/user_management/police_manage/sign_in', this.state);
        console.log(response.data);
        if (response.data != null) {
            if (response.data.status === 200) {
                if (response.data.msg === "Session success") {
                    sessionStorage.setItem('USER_AUTH', JSON.stringify({
                        jwt: response.data.jwt,
                        data: response.data.data
                    }));
                    // var data = sessionStorage.getItem('key');
                    this.props.history.push("/");
                } else {
                    alert(response.data.msg);
                }
            } else if (response.data.status === 500) {
                alert("Tenemos un percance, esperenos a arreglar el incidente y vuelva nuevamente a intentar porfavor");
            }
        }
    }

    render() {
        return (
            <div>
                <NavBar />
                <div className="p-2">
                    <Card
                        bg="dark"
                        text="white"
                        style={{
                            float: 'none',
                            margin: '0 auto',
                            width: '60rem',
                        }}
                        className="container mt-5"
                    >
                        <Card.Header>Login</Card.Header>
                        <Row>
                            <Image className="col-md-4" src={loginPana} />
                            <Form
                                className="col-md-4"
                                onSubmit={this.onSubmitSignIn}
                            >
                                <Form.Group className="mb-3" controlId="formBasicEmail">
                                    <Form.Label>Email</Form.Label>
                                    <Form.Control type="email" placeholder="Enter email" onChange={this.onChangeEmail} />
                                    <Form.Text className="text-muted">
                                        No compartiremos su email con nadie.
                                    </Form.Text>
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="formBasicPassword">
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control type="password" placeholder="Password" onChange={this.onChangePassword} />
                                </Form.Group>
                                <Button variant="primary" type="submit">
                                    Sign In
                                </Button>
                                <Nav.Link href="/restore_account">Olvidaste tu contraseña</Nav.Link>
                            </Form>
                        </Row>
                    </Card>
                </div>
                <Footer />
            </div>
        )
    }
}
