import React, { Component } from 'react'
import { Button, Card, Form } from 'react-bootstrap'
import Footer from '../../../template/Footer'
import NavBar from '../../../template/NavBar'

export default class NewPassword extends Component {
    render() {
        return (
            <div>
                <NavBar />
                <div>
                    <Card
                        bg="dark"
                        text="white"
                        style={{
                            width: '30rem',
                        }}
                        className="container mt-5"
                    >
                        <Card.Header>Restauración de contraseña</Card.Header>
                        <Form className="p-3">
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>Nueva contraseña</Form.Label>
                                <Form.Control type="password" placeholder="Enter password" />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>Repetir nueva contraseña</Form.Label>
                                <Form.Control type="password" placeholder="retype password" />
                            </Form.Group>
                            <Button variant="primary" type="submit">
                                Confirmar contraseña
                            </Button>
                        </Form>
                    </Card>
                </div>
                <Footer />
            </div>
        )
    }
}
