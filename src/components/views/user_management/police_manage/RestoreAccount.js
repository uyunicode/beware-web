import React, { Component } from 'react'
import { Button, Card, Form } from 'react-bootstrap'
import { HTTP } from '../../../../services/http'
import Footer from '../../../template/Footer'
import NavBar from '../../../template/NavBar'

export default class RestoreAccount extends Component {

    state = {
        email: ""
    }

    onChangeEmail = (e) => {
        this.setState({ email: e.target.value });
    }

    onSubmitRestoreAccount = async (e) => {
        e.preventDefault();
        console.log(this.state.email);
        let response = await HTTP.post(`/user_management/police_manage/restore_account`, {
            email: this.state.email
        });
        console.log(response);
    }


    render() {
        return (
            <div>
                <NavBar />
                <div>
                    <Card
                        bg="dark"
                        text="white"
                        style={{
                            width: '30rem',
                        }}
                        className="container mt-5"
                    >
                        <Card.Header>Restaurar cuenta</Card.Header>
                        <Form className="p-3" onSubmit={this.onSubmitRestoreAccount} >
                            <p>Este proceso enviara un correo de seguridad si el email esta registrado,
                                el email le llevara a la restauración de la cuenta
                            </p>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>Email de su cuenta</Form.Label>
                                <Form.Control type="email" placeholder="Enter email" onChange={this.onChangeEmail} />
                            </Form.Group>
                            <Button variant="primary" type="submit">
                                Enviar email
                            </Button>
                        </Form>
                    </Card>
                </div>
                <Footer />
            </div>
        )
    }
}
