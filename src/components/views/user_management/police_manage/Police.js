import React, { Component } from 'react'
import { Accordion, Badge, Button, Card, Col, Form, Row} from 'react-bootstrap'
import { HTTP } from '../../../../services/http'
import Footer from '../../../template/Footer'
import NavBar from '../../../template/NavBar'

export default class Police extends Component {

    state = {
        list_user_police: [],
        ci: 0,
        name: "",
        cellphone: "",
        email: "",
        password: "",
        ladder_number: "",
        grade: "Policia"
    }

    async componentDidMount() {
        await this.getListPoliceUser();
    }

    getListPoliceUser = async () => {
        let listUserPoliceAccount = await HTTP.get('/user_management/police_manage/list_police_user');
        // console.log(listUserPoliceAccount);
        if (listUserPoliceAccount) {
            if (listUserPoliceAccount.data.status !== 500) {
                this.setState({ list_user_police: listUserPoliceAccount.data.data });
            } else {
                alert("Hubo un error al cargar los datos, intente de nuevo porfavor");
            }
        } else {
            console.log("Error");
        }
    }

    onChangeCI = (event) => {
        this.setState({ ci: event.target.value });
    }

    onChangeName = (event) => {
        this.setState({ name: event.target.value });
    }

    onChangeCellphone = (event) => {
        this.setState({ cellphone: event.target.value });
    }

    onChangeEmail = (event) => {
        this.setState({ email: event.target.value });
    }

    onChangePassword = (event) => {
        this.setState({ password: event.target.value });
    }

    onChangeLadderNumber = (event) => {
        this.setState({ ladder_number: event.target.value });
    }

    OnChangeGrade = (event) => {
        this.setState({ grade: event.target.value });
    }

    CleanForm = () => {
        this.setState({ci: "", name: "", cellphone: "", email: "", password: "", ladder_number: "", grade: "Policia"});
    }

    onSubmitSignUp = async event => {
        event.preventDefault();
        console.log(this.state);
        let response = await HTTP.post('/user_management/police_manage/sign_up', {
            ci: this.state.ci, 
            name: this.state.name, 
            cellphone: `+591${this.state.cellphone}`, 
            email: this.state.email, 
            password: this.state.password, 
            state: true, 
            type_user: false, 
            ladder_number: this.state.ladder_number, 
            grade: this.state.grade
        });
        console.log(response.data);
        if (response.data != null) {
            if (response.data.status === 200) {
                alert(`Usuario ${this.state.name} creado exitosamente!`);
                await this.getListPoliceUser();
                this.CleanForm();                
            } else if (response.data.status === 500) {
                alert(`Ocurrio un error al registrar al usuario ${this.state.name}, intente nuevamente`);
            }
        }else{
            alert(`Ocurrio un error al registrar al usuario ${this.state.name}, intente nuevamente`);
        }
    }

    render() {
        return (
            <>
                <NavBar />
                <div className="p-3">
                    <Accordion defaultActiveKey="0"> 
                        <Card bg="dark" className="text-light">
                            <Accordion.Toggle as={Card.Header} eventKey="1">
                                Crear cuenta usuario policia
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="1">
                                <Card.Body>
                                    <Form onSubmit={this.onSubmitSignUp}>
                                        <Row className="mb-3">
                                            <Form.Group as={Col} md="4" controlId="validationCustom01">
                                                <Form.Label>CI</Form.Label>
                                                <Form.Control
                                                    required
                                                    type="number"
                                                    placeholder="6767136"
                                                    onChange={this.onChangeCI}
                                                    value={this.state.ci}
                                                />
                                                <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                                            </Form.Group>
                                            <Form.Group as={Col} md="4" controlId="validationCustom02">
                                                <Form.Label>Nombre Completo</Form.Label>
                                                <Form.Control
                                                    required
                                                    type="text"
                                                    placeholder="Jeyson Chura"
                                                    onChange={this.onChangeName}
                                                    value={this.state.name}
                                                />
                                                <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                                            </Form.Group>
                                            <Form.Group as={Col} md="4" controlId="validationCustom03">
                                                <Form.Label>Teléfono</Form.Label>
                                                <Form.Control
                                                    required
                                                    type="text"
                                                    placeholder="75555555"
                                                    onChange={this.onChangeCellphone}
                                                    value={this.state.cellphone}
                                                />
                                                <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                                            </Form.Group>
                                        </Row>
                                        <Row className="mb-3">
                                            <Form.Group as={Col} md="4" controlId="validationCustomUsername">
                                                <Form.Label>Email</Form.Label>
                                                <Form.Control
                                                    type="email"
                                                    placeholder="@gmail.com"
                                                    required
                                                    onChange={this.onChangeEmail}
                                                    value={this.state.email}
                                                />
                                            </Form.Group>
                                            <Form.Group as={Col} md="4" controlId="validationCustom05">
                                                <Form.Label>Password</Form.Label>
                                                <Form.Control
                                                    required
                                                    type="password"
                                                    placeholder="contraseña"
                                                    onChange={this.onChangePassword}
                                                    value={this.state.password}
                                                />
                                                <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                                            </Form.Group>
                                            <Form.Group as={Col} md="4" controlId="validationCustom06">
                                                <Form.Label>Nro de escalafón</Form.Label>
                                                <Form.Control
                                                    required
                                                    type="text"
                                                    placeholder="CE-9821"
                                                    onChange={this.onChangeLadderNumber}
                                                    value={this.state.ladder_number}
                                                />
                                                <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                                            </Form.Group>
                                        </Row>
                                        <Row className="mb-3">
                                            <Form.Group controlId="exampleForm.SelectCustom">
                                                <Form.Label>Grado:  </Form.Label>
                                                <Form.Control as="select" custom onChange={this.OnChangeGrade} value={this.state.grade}>
                                                    <option value="Policia" selected>Policia</option>
                                                    <option value="Cabo">Cabo</option>
                                                    <option value="Sargento">Sargento</option>
                                                    <option value="Suboficial">Suboficial</option>
                                                    <option value="Cadete">Cadete</option>
                                                    <option value="Teniente">Teniente</option>
                                                    <option value="Capitan">Capitan</option>
                                                    <option value="Coronel">Coronel</option>
                                                    <option value="General">General</option>
                                                    <option value="Comandante">Comandante</option>
                                                </Form.Control>
                                            </Form.Group>
                                        </Row>
                                        <Button variant="primary" type="submit">
                                            Crear cuenta policia
                                        </Button>
                                    </Form>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card bg="dark" className="text-light">
                            <Accordion.Toggle as={Card.Header} eventKey="0">
                                Lista de usuarios policia
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                    <Row xs={1} md={4} className="g-4">
                                        {this.state.list_user_police.map((userPolice, idx) => (
                                            <Col>
                                                <Card bg="dark" border="primary" style={{ width: '18rem' }}>
                                                    <Card.Header>CI: {userPolice.ci} </Card.Header>
                                                    <Card.Body>
                                                        <Card.Title>{userPolice.name}</Card.Title>
                                                        <Card.Text>
                                                            Telf. {userPolice.cellphone}<br />
                                                            Email: {userPolice.email}<br />
                                                            <div>
                                                                Estado: <Badge pill bg={userPolice.state ? "warning" : "danger"}>{userPolice.state ? "Habilitado" : "Desabilitado"}</Badge><br />
                                                            </div>
                                                            Nro. escalafón: {userPolice.ladder_number}<br />
                                                            Grado: {userPolice.grade}<br />
                                                        </Card.Text>
                                                    </Card.Body>
                                                </Card>
                                            </Col>
                                        ))}
                                    </Row>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                    </Accordion>
                </div>
                <Footer />
            </>
        )
    }
}
