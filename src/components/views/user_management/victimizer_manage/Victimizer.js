import React, { Component } from 'react'

import { Accordion, Button, Card, Col, Form, Row, Image } from 'react-bootstrap'
import { HTTP } from '../../../../services/http'
import Footer from '../../../template/Footer'
import NavBar from '../../../template/NavBar'

export default class Victimizer extends Component {
    state = {
        victimizerList: [],
        ci: 0,
        name: "",
        profile: "",
        urlProfile: ""
    }

    async componentDidMount() {
        await this.getVictimizerList();
    }

    getVictimizerList = async () => {
        let victimizerList = await HTTP.get('/user_management/victimizer_manage/get_list_victimizer');
        if (victimizerList) {
            if (victimizerList.data.status !== 500) {
                this.setState({ victimizerList: victimizerList.data.data });
            } else {
                alert("Hubo un error al cargar los datos, intente de nuevo porfavor");
            }
        } else {
            console.log("Error");
        }
    }

    onChangeCI = (event) => {
        this.setState({ ci: event.target.value });
    }

    onChangeName = (event) => {
        this.setState({ name: event.target.value });
    }

    onChangeProfile = (event) => {
        console.log(`File: ${event.target.files[0]}`);
        this.setState({ profile: event.target.files[0], urlProfile: URL.createObjectURL(event.target.files[0]) });
    }

    CleanForm = () => {
        this.setState({ ci: "", name: "", urlProfile: "" });
    }

    onSubmitRegisterVictimizer = async event => {
        event.preventDefault();
        const formData = new FormData();
        formData.append('ci', this.state.ci);
        formData.append('name', this.state.name);
        formData.append('profile', this.state.profile);
        let response = await HTTP.postFile('/user_management/victimizer_manage/add_victimizer', formData);
        console.log(response.data);
        if (response.data != null) {
            if (response.data.status === 200) {
                alert(`Victimizador ${this.state.name} creado exitosamente!`);
                await this.getVictimizerList();
                this.CleanForm();
            } else if (response.data.status === 500) {
                alert(`Ocurrio un error al registrar al victimizador ${this.state.name}, intente nuevamente`);
            }
        } else {
            alert(`Ocurrio un error al registrar al victimizador ${this.state.name}, intente nuevamente`);
        }
    }
    render() {
        return (
            <>
                <NavBar />
                <div className="p-3">
                    <Accordion defaultActiveKey="0">
                        <Card bg="dark" className="text-light">
                            <Accordion.Toggle as={Card.Header} eventKey="1">
                                Registrar Victimario
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="1">
                                <Card.Body>
                                    <Form onSubmit={this.onSubmitRegisterVictimizer} >
                                        <Row md="4">
                                            <Image src={this.state.urlProfile} rounded thumbnail />
                                        </Row>
                                        <Row>
                                            <Form.Group as={Col} md="4" controlId="validationCustom03">
                                                <Form.Label>Perfil</Form.Label>
                                                <Form.File
                                                    id="custom-file"
                                                    custom
                                                    onChange={this.onChangeProfile}
                                                    required
                                                />
                                                <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                                            </Form.Group>
                                        </Row>
                                        <Row className="mb-2">
                                            <Form.Group as={Col} md="6" controlId="validationCustom01">
                                                <Form.Label>CI</Form.Label>
                                                <Form.Control
                                                    required
                                                    type="number"
                                                    placeholder="6767136"
                                                    onChange={this.onChangeCI}
                                                    value={this.state.ci}
                                                />
                                                <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                                            </Form.Group>
                                            <Form.Group as={Col} md="6" controlId="validationCustom02">
                                                <Form.Label>Nombre Completo</Form.Label>
                                                <Form.Control
                                                    required
                                                    type="text"
                                                    placeholder="Jeyson Chura"
                                                    onChange={this.onChangeName}
                                                    value={this.state.name}
                                                />
                                                <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
                                            </Form.Group>
                                        </Row>
                                        <Button variant="primary" type="submit">
                                            Registrar
                                        </Button>
                                    </Form>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card bg="dark" className="text-light">
                            <Accordion.Toggle as={Card.Header} eventKey="0">
                                Lista de Victimarios
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="0">
                                <Card.Body>
                                    <Row xs={1} md={4} className="g-4">
                                        {this.state.victimizerList.map((victimizer) => (
                                            <Col>
                                                <Card bg="dark" border="primary" style={{ width: '18rem' }}>
                                                    <Card.Img src={`https://beware-app.s3.us-east-2.amazonaws.com/victimizer-profiles/${victimizer.profile}`} variant="top"/>
                                                    <Card.Body>
                                                        <Card.Header>CI: {victimizer.ci} </Card.Header>
                                                        <Card.Title>{victimizer.name}</Card.Title>
                                                    </Card.Body>
                                                </Card>
                                            </Col>
                                        ))}
                                    </Row>
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                    </Accordion>
                </div>
                <Footer />
            </>
        );
    }
}