import React, { Component } from 'react';

import Footer from '../../../template/Footer';
import NavBar from '../../../template/NavBar';
import { Card, Col, Row, Form, Button, Image } from 'react-bootstrap';
import { HTTP } from '../../../../services/http';
import noimage from '../../../assets/noimage.jpg';
import {Link} from 'react-router-dom';

export default class RequestHelpDetail extends Component {

    state = {
        requestHelp: {},
        victimizerSimilarity: 0.00
    }

    async componentDidMount() {
        let code = this.props.match.params.id;
        await this.getVictimizerList(code);
    }

    getVictimizerList = async (code) => {
        let requestData = await HTTP.get(`/request_help_management/request_help_manage/${code}`);
        if (requestData) {
            if (requestData.data.status !== 500) {
                this.setState({ requestHelp: requestData.data.data });
            } else {
                alert("Error al cargar los datos, intente nuevamente");
            }
        } else {
            console.log("Error");
        }
    }

    isToday = requestHelpDate => {
        let dateNow = new Date().toLocaleString("en-US", { timeZone: "America/La_Paz" });
        let dateNowLocale = new Date(dateNow);
        let requestHelpDateTime = new Date(requestHelpDate);
        return dateNowLocale.toLocaleDateString() === requestHelpDateTime.toLocaleDateString();
    }

    quantityPhotos = () => {
        let c = 0;
        if(this.state.requestHelp.filename1!=="a.jpg") c++;
        if(this.state.requestHelp.filename2!=="b.jpg") c++;
        return c;
    }

    isPossibleToCompare = () => {
        if(this.state.requestHelp.ci_victimizer === 0){
            return this.quantityPhotos() > 0
        }
        return false;
    }

    onSubmitCompareFaces = async event => {
        event.preventDefault();
        let response = await HTTP.put(`/request_help_management/request_help_manage/update_ci_victimizer/${this.state.requestHelp.code}`, {
            filename1: this.state.requestHelp.filename1,
            filename2: this.state.requestHelp.filename2
        });
        response=response.data;
        if (response.status === 200) {       
            if(typeof response.data.victimizer != "undefined"){
                alert(`Se han encontrado resultados!\n La o las fotos enviadas por la posible victima coinciden con el victimazador: ${response.data.victimizer.ci}`);
                await this.getVictimizerList(this.state.requestHelp.code);
                this.setState({victimizerSimilarity: response.data.similarity});
            }else{
                alert("No se han encontrado coincidencias");
            }
        } else if (response.status === 500) {
            alert("Error al comparar, intente nuevamente");
        }
    }

    render() {
        return (
            <>
                <NavBar />
                <div className="p-3">
                    <Card bg="dark" className="text-light">
                        <Card.Body>
                            <Card.Header>Código de Solicitud de Auxilio: {this.state.requestHelp.code} </Card.Header>
                            <Row xs={1} md={1} className="g-1">
                                <Card bg="dark" style={{ width: '81rem' }}>
                                    <Card.Body>
                                        <Card.Title>{this.state.requestHelp.name}</Card.Title>
                                        <Card.Text>
                                            CI: {this.state.requestHelp.ci_victim}
                                            <div>
                                                Fecha: <span className={this.isToday(this.state.requestHelp.date_time) ? "p-1 mb-1 bg-success text-white" : "p-1 mb-1 bg-warning text-white"}>
                                                    {this.isToday(this.state.requestHelp.date_time) ? `Hoy, ${new Date(this.state.requestHelp.date_time).toLocaleTimeString()}` : new Date(this.state.requestHelp.date_time).toLocaleString()}
                                                </span>
                                            </div>
                                            Telf. {this.state.requestHelp.cellphone}<br />
                                            Ubicación: {this.state.requestHelp.initial_location}<br />
                                            <Link to="/request_management/request_map">Ver Ubicación</Link>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </Row>
                            <Row xs={1} md={1} className="g-1">
                                <Card bg="dark" border="primary" style={{ width: '81rem' }}>
                                    <Card.Body>
                                        <Card.Title>
                                            Comparación de Rostros: <span className={this.state.requestHelp.ci_victimizer > 0 ? "p-1 mb-1 bg-success text-white" : "p-1 mb-1 bg-danger text-white"}>
                                                {this.state.requestHelp.ci_victimizer > 0 ? "SI" : "NO"}
                                            </span><br/>
                                            Fotografías: <span className={this.quantityPhotos()>0? "p-1 mb-1 bg-success text-white" : "p-1 mb-1 bg-danger text-white"}>
                                                {this.quantityPhotos()}
                                            </span>
                                            <br/><br/>
                                        </Card.Title>
                                        <Row xs= {1} md= {3}>
                                            <Col>
                                                <Image src={this.state.requestHelp.filename1==="a.jpg"? noimage:`https://beware-app.s3.us-east-2.amazonaws.com/request-help-files/${this.state.requestHelp.filename1}`} 
                                                    thumbnail rounded/>
                                                Fotografía 1
                                            </Col>
                                            
                                            <Col>
                                                <Image src={this.state.requestHelp.filename2==="b.jpg"? noimage:`https://beware-app.s3.us-east-2.amazonaws.com/request-help-files/${this.state.requestHelp.filename2}`} 
                                                    thumbnail rounded/>
                                                Fotografía 2
                                            </Col>
                                            <Col>
                                                <Image src={this.state.requestHelp.ci_victimizer > 0? `https://beware-app.s3.us-east-2.amazonaws.com/victimizer-profiles/${this.state.requestHelp.profile}`: noimage} 
                                                    thumbnail rounded/>
                                                Victimizador: {this.state.requestHelp.ci_victimizer > 0 ? this.state.requestHelp.victimizer_name: "Sin Detectar Aún"}<br/>
                                                CI: {this.state.requestHelp.ci_victimizer > 0 ? this.state.requestHelp.ci_victimizer: "Sin Detectar Aún"}<br/>
                                                Similitud: {this.state.victimizerSimilarity}%
                                            </Col>
                                        </Row>
                                    </Card.Body>
                                    <Card.Footer>
                                        <Form onSubmit={this.onSubmitCompareFaces}>
                                            <Button variant="primary" type="submit" disabled={this.isPossibleToCompare()?false: true}>Realizar Comparación</Button>
                                        </Form>
                                        <br/><br/><br/>
                                    </Card.Footer>    
                                </Card>
                            </Row>

                        </Card.Body>

                    </Card>
                </div>
                <Footer />
            </>
        )
    }
}
