import React, { Component } from 'react';

import Footer from '../../../template/Footer';
import NavBar from '../../../template/NavBar';
import {Link} from 'react-router-dom';
import {Card, Col, Row} from 'react-bootstrap';
import { HTTP } from '../../../../services/http';

export default class RequestHelp extends Component {
    
    state = {
        requestHelpList: [],
    }

    async componentDidMount() {
        await this.getVictimizerList();
    }

    getVictimizerList = async () => {
        let requestList = await HTTP.get('/request_help_management/request_help_manage/request_help_list');
        if (requestList) {
            if (requestList.data.status !== 500) {
                this.setState({ requestHelpList: requestList.data.data });
            } else {
                alert("Error al cargar los datos, intente nuevamente");
            }
        } else {
            console.log("Error");
        }
    }


    onSubmitRegisterVictimizer = async event => {
        event.preventDefault();
        const formData = new FormData();
        formData.append('ci', this.state.ci);
        formData.append('name', this.state.name);
        formData.append('profile', this.state.profile);
        let response = await HTTP.postFile('/user_management/victimizer_manage/add_victimizer', formData);
        console.log(response.data);
        if (response.data != null) {
            if (response.data.status === 200) {
                alert(`Victimizador ${this.state.name} creado exitosamente!`);
                await this.getVictimizerList();
                this.CleanForm();
            } else if (response.data.status === 500) {
                alert(`Ocurrio un error al registrar al victimizador ${this.state.name}, intente nuevamente`);
            }
        } else {
            alert(`Ocurrio un error al registrar al victimizador ${this.state.name}, intente nuevamente`);
        }
    }

    isToday = requestHelpDate =>{
        let dateNow = new Date().toLocaleString("en-US", {timeZone: "America/La_Paz"});
        let dateNowLocale = new Date(dateNow);
        let requestHelpDateTime = new Date(requestHelpDate);
        return dateNowLocale.toLocaleDateString() === requestHelpDateTime.toLocaleDateString();
    }
    render() {
        return (
            <>
                <NavBar />
                <div className="p-3">
                    <Card bg="dark" className="text-light">
                        <Card.Body>
                            <Row xs={1} md={4} className="g-4">
                                {this.state.requestHelpList.map((requestHelp) => (
                                    <Col>
                                        <Card bg="dark" border="primary" style={{ width: '18rem' }}>
                                            <Card.Body>
                                                <Card.Header>CI: {requestHelp.ci_victim} </Card.Header>
                                                <Card.Title>{requestHelp.name}</Card.Title>
                                                <Card.Text>
                                                    <div>
                                                        Fecha: <span className={this.isToday(requestHelp.date_time)?"p-1 mb-1 bg-success text-white": "p-1 mb-1 bg-warning text-white"}>
                                                                    {this.isToday(requestHelp.date_time)? `Hoy, ${new Date(requestHelp.date_time).toLocaleTimeString()}` : new Date(requestHelp.date_time).toLocaleString()}
                                                               </span>
                                                    </div>
                                                    Telf. {requestHelp.cellphone}<br />
                                                    Ubicación: {requestHelp.initial_location}<br />
                                                    <div>
                                                        Comparación de Rostros: <span className={requestHelp.ci_victimizer>0?"p-1 mb-1 bg-success text-white": "p-1 mb-1 bg-danger text-white"}>
                                                                                    {requestHelp.ci_victimizer>0?"SI" : "NO"}
                                                                                </span>
                                                    </div>
                                                </Card.Text>
                                                <Card.Footer> 
                                                    <Row>
                                                        <Link to="/request_management/request_map">Ver Ubicación</Link>
                                                    </Row>
                                                    <Row>
                                                        <Link to={`/request_management/detail/${requestHelp.code}`}>Detalles</Link>
                                                    </Row>
                                                </Card.Footer>
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                ))}
                            </Row>
                        </Card.Body>
                    </Card>
                </div>
                <Footer />
            </>
        )
    }
}
