import React, { Component } from 'react'
import Footer from '../../../template/Footer'
import NavBar from '../../../template/NavBar'
import { Map, Marker, Popup, TileLayer } from 'react-leaflet'
import LocationIconVenue from '../../../assets/venue_location_icon.svg';
import L from "leaflet";
import "leaflet/dist/leaflet.css";
import { SocketClient } from '../../../../services/socket-io-client'


export default class RequestMap extends Component {
    state = {
        position: [-17.783251, -63.182012],
        currentLocation: { lat: 52.52437, lng: 13.41053 },
        zoom: 80
    }

    componentDidMount() {
        var client = new SocketClient();
        client.socket.on('position', (data) => {
            console.log(`Position: ${data.data.latitude}`);
            this.setState({ position: [data.data.latitude, data.data.longitude] });
        });
        client.socket.on('notification', (data) => {
            console.log(`Notification: ${data}`);
        });
    }
    render() {
        return (
            <div>
                <NavBar />
                <Map center={this.state.position} zoom={16}>
                    <TileLayer
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                        attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
                    />
                    <Marker position={this.state.position} icon={L.icon({
                        iconUrl: LocationIconVenue,
                        iconRetinaUrl: LocationIconVenue,
                        iconAnchor: null,
                        shadowUrl: null,
                        shadowSize: null,
                        shadowAnchor: null,
                        iconSize: [35, 35],
                        className: "leaflet-venue-icon",
                    })}>
                        <Popup>A pretty CSS3 popup.<br />Easily customizable.</Popup>
                    </Marker>
                </Map>
                <Footer />
            </div>
        )
    }
}
