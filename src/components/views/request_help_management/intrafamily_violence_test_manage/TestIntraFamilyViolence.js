import React, { Component } from 'react'
import { Button, ButtonGroup, Card, Col, Row, Tab, Tabs } from 'react-bootstrap';
import { HTTP } from '../../../../services/http'
import NavBar from '../../../template/NavBar';

export default class TestIntraFamilyViolence extends Component {

    state = {
        list_questions: [],
        list_verify_responses: [],
        list_responses_first_phase: [],
        list_responses_second_phase: [],
        list_responses_third_phase: []
    }

    async componentDidMount() {
        let listQuestions = await HTTP.get("/request_help_management/intrafamily_violence_test_manage/get_questions_test");
        let listResponses = [];
        for (let index = 0; index < listQuestions.data.data.length; index++) {
            listResponses.push(false);
        }
        if (listQuestions) {
            this.setState({
                list_questions: listQuestions.data.data,
                list_verify_responses: listResponses
            });
        }
    }

    /**
     * add answer of the questions test
     * @param {*} value : value response {nunca, en ocasiones, casi siempre}
     * @param {*} indexQuestion : index question in the list
     * @param {*} indexPhase : index phase list
     */
    addAnswer = (value, indexQuestion, indexPhase) => {
        // console.table({ value, indexQuestion, indexPhase, question: this.state.list_questions[indexQuestion - 1] });
        let listResponseFront = this.state.list_verify_responses;
        if (indexPhase === 1) {
            let listPhase = this.state.list_responses_first_phase;
            for (let index = 0; index < listPhase.length; index++) {
                let element = listPhase[index];
                if (element.index_question === indexQuestion) {
                    listPhase[index] = {
                        index_question: indexQuestion,
                        value
                    };
                    listResponseFront[indexQuestion - 1] = true;
                    this.setState({
                        list_responses_first_phase: listPhase,
                        list_verify_responses: listResponseFront
                    });
                    return;
                }
            }
            listPhase.push({
                index_question: indexQuestion,
                value
            });
            listResponseFront[indexQuestion - 1] = true;
            this.setState({ list_responses_first_phase: listPhase, list_verify_responses: listResponseFront });
        } else if (indexPhase === 2) {
            let listPhase = this.state.list_responses_second_phase;
            for (let index = 0; index < listPhase.length; index++) {
                let element = listPhase[index];
                if (element.index_question === indexQuestion) {
                    listPhase[index] = {
                        index_question: indexQuestion,
                        value
                    };
                    listResponseFront[indexQuestion - 1] = true;
                    this.setState({ list_responses_second_phase: listPhase, list_verify_responses: listResponseFront });
                    return;
                }
            }
            listPhase.push({
                index_question: indexQuestion,
                value
            });
            listResponseFront[indexQuestion - 1] = true;
            this.setState({ list_responses_second_phase: listPhase, list_verify_responses: listResponseFront });
        } else if (indexPhase === 3) {
            let listPhase = this.state.list_responses_third_phase;
            for (let index = 0; index < listPhase.length; index++) {
                let element = listPhase[index];
                if (element.index_question === indexQuestion) {
                    listPhase[index] = {
                        index_question: indexQuestion,
                        value
                    };
                    listResponseFront[indexQuestion - 1] = true;
                    this.setState({
                        list_responses_third_phase: listPhase,
                        list_verify_responses: listResponseFront
                    });
                    return;
                }
            }
            listPhase.push({
                index_question: indexQuestion,
                value
            });
            listResponseFront[indexQuestion - 1] = true;
            this.setState({
                list_responses_third_phase: listPhase,
                list_verify_responses: listResponseFront
            });
        } else {
            throw new Error("Invalid indexPhase");
        }
    }

    onClickTestViolence = async () => {

        let listResponses = [...this.state.list_responses_first_phase, ...this.state.list_responses_second_phase, ...this.state.list_responses_third_phase]
        if (listResponses.length === 33) {
            let listToTestBackend = [];
            for (let index = 0; index < listResponses.length; index++) {
                listToTestBackend.push(listResponses[index].value);
            }
            let responseTest = await HTTP.post('/request_help_management/intrafamily_violence_test_manage/test_answers', {
                answers: listToTestBackend
            });
            console.log(responseTest);
            if (responseTest) {
                if (responseTest.data.data === 1) {
                    //phase1
                    alert("FASE 1: Debes reconocer que te encuentras ya en las primeras fases del ciclo. Puedes informarte sobre lo que significa el Ciclo de la violencia y de qué modos puedes salir; siempre debes evitar aislarte de personas de tu confianza, y debes comprender que sufrir violencia no es un tema para avergonzarse, sino que al contrario, es una situación en la que se debe solicitar ayuda.")
                } else if (responseTest.data.data === 2) {
                    alert("FASE 2: estás enfrentando un nivel más avanzado y visible de violencia. En este punto tu autoestima se verá afectada, te sentirás confundida y creerás que el maltrato es parte de una relación de afecto. Es complejo salir del Ciclo porque muchas veces pensamos que la persona agresora reacciona de esa manera por nuestra culpa o porque tuvo un pasado difícil, lo que nos hace crearnos la falsa expectativa de que nuestro amor y atención incondicionales pueden hacer que la persona agresora cambie. En este punto debemos prestar atención a todas las formas en las que puede manifestarse la violencia, como son la indiferencia afectiva, el control sobre nuestros bienes y sobre nuestra libertad personal. Podemos creer que renunciar a nuestros derechos es una muestra de amor a la otra persona, cuando realmente es una aceptación sumisa de la capacidad de dominación de la otra persona sobre nosotras.");
                } else if (responseTest.data.data === 0) {
                    alert("Segun a sus respuestas de esta encuesta, usted no sufre violencia intrafamiliar, mantenga una buena comunicación de");
                } else {
                    alert("FASE 3: Debes tener presente que tu vida corre un inminente peligro. Nunca es muy tarde para pedir ayuda.");
                }
            } else {
                alert("Por favor vuelva a enviar su test");
            }
        } else {
            alert("Termine de llenar el cuestionario por favor, para que tengamos una evaluación mas optima");
        }
    }

    render() {
        return (
            <div>
                <NavBar />
                <div className="p-4">
                    <Card
                        bg="dark"
                        text="white"
                        style={{
                            width: '60rem',
                        }}
                        className="container"
                    >
                        <Card.Body>
                            <Tabs defaultActiveKey="fase-1" id="uncontrolled-tab-example" variant="dark" className="container">
                                <Tab eventKey="fase-1" title="Fase de Acumulación de Tensión">
                                    <div className="p-3">
                                        <h4>Fase de Acumulación de Tensión</h4>
                                        {this.state.list_questions.slice(0, 10).map((question, idx) => {
                                            return (
                                                <Card style={{
                                                    border: '1px dashed #2db944',
                                                    borderRadius: '5px'
                                                }}
                                                    bg="dark"
                                                    text="white">
                                                    <Card.Header>{idx + 1}: {question}</Card.Header>
                                                    <ButtonGroup aria-label="Basic example">
                                                        <Button variant="success" disabled={this.state.list_verify_responses[idx]} onClick={() => this.addAnswer(1, idx + 1, 1)}>Nunca</Button>
                                                        <Button variant="success" disabled={this.state.list_verify_responses[idx]} onClick={() => this.addAnswer(2, idx + 1, 1)}>En ocasiones</Button>
                                                        <Button variant="success" disabled={this.state.list_verify_responses[idx]} onClick={() => this.addAnswer(3, idx + 1, 1)}>Casi siempre</Button>
                                                    </ButtonGroup>
                                                </Card>
                                            )
                                        })}
                                    </div>
                                </Tab>
                                <Tab eventKey="fase-2" title="Fase del Episodio Agudo">
                                    <div className="p-3">
                                        <h4>Fase del Episodio Agudo</h4>
                                        {this.state.list_questions.slice(10, 21).map((question, idx) => (
                                            <Card style={{
                                                border: '1px dashed #dbff1e',
                                                borderRadius: '5px'
                                            }}
                                                bg="dark"
                                                text="white">
                                                <Card.Header>{idx + 11}: {question}</Card.Header>
                                                <ButtonGroup aria-label="Basic example">
                                                    <Button variant="warning" disabled={this.state.list_verify_responses[idx + 10]} onClick={() => this.addAnswer(1, idx + 11, 2)}>Nunca</Button>
                                                    <Button variant="warning" disabled={this.state.list_verify_responses[idx + 10]} onClick={() => this.addAnswer(2, idx + 11, 2)}>En ocasiones</Button>
                                                    <Button variant="warning" disabled={this.state.list_verify_responses[idx + 10]} onClick={() => this.addAnswer(3, idx + 11, 2)}>Casi siempre</Button>
                                                </ButtonGroup>
                                            </Card>
                                        ))}
                                    </div>
                                </Tab>
                                <Tab eventKey="fase-3" title="Fase de la Luna de Miel o reconciliatorio">
                                    <div className="p-3">
                                        <h4>Fase de la Luna de Miel o reconciliatorio</h4>
                                        {this.state.list_questions.slice(21, 33).map((question, idx) => (
                                            <Card style={{
                                                border: '1px dashed #ff1e32',
                                                borderRadius: '5px'
                                            }}
                                                bg="dark"
                                                text="white">
                                                <Card.Header>{idx + 22}: {question}</Card.Header>
                                                <ButtonGroup aria-label="Basic example">
                                                    <Button variant="danger" disabled={this.state.list_verify_responses[idx + 21]} onClick={() => this.addAnswer(1, idx + 22, 3)}>Nunca</Button>
                                                    <Button variant="danger" disabled={this.state.list_verify_responses[idx + 21]} onClick={() => this.addAnswer(2, idx + 22, 3)}>En ocasiones</Button>
                                                    <Button variant="danger" disabled={this.state.list_verify_responses[idx + 21]} onClick={() => this.addAnswer(3, idx + 22, 3)}>Casi siempre</Button>
                                                </ButtonGroup>
                                            </Card>
                                        ))}
                                    </div>
                                </Tab>
                            </Tabs>
                        </Card.Body>
                        <Card.Footer>
                            <Row><Button variant="primary" block onClick={this.onClickTestViolence}>Evaluar Test</Button></Row>
                        </Card.Footer>
                    </Card>
                </div>
                <footer style={{
                    position: 'relative',
                    left: '0',
                    bottom: '0',
                    width: '100%'
                }} className="text-center bg-dark text-white">
                    <div className="text-center p-3">
                        <Row>
                            <Col>
                                😁😁 2021 Copyright: -
                                <a className="text-white" href="https://mdbootstrap.com/">uyunicode.com</a>
                            </Col>
                            <Col></Col>
                            <Col>
                                <a href="https://storyset.com/people">People illustrations by Storyset</a>
                            </Col>
                        </Row>
                    </div>
                    {/* Copyright */}
                </footer>
            </div >
        )
    }
}
