import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from './components/views/Home'
import RequestMap from './components/views/request_help_management/request_help_manage/RequestMap';
import Login from './components/views/user_management/police_manage/Login';
import Police from './components/views/user_management/police_manage/Police';
import Victimizer from './components/views/user_management/victimizer_manage/Victimizer';
import RequestHelp from './components/views/request_help_management/request_help_manage/RequestHelp';
import RequestHelpDetail from './components/views/request_help_management/request_help_manage/RequestHelpDetail';

import './App.css';
import RestoreAccount from './components/views/user_management/police_manage/RestoreAccount';
import NewPassword from './components/views/user_management/police_manage/NewPassword';
import TestIntraFamilyViolence from './components/views/request_help_management/intrafamily_violence_test_manage/TestIntraFamilyViolence';
function App() {
  return (
    <Router>
      <Route path="/" exact component={Home}></Route>
      <Route path="/login" exact component={Login}></Route>
      <Route path="/police_manage" exact component={Police}></Route>
      <Route path="/restore_account" exact component={RestoreAccount}></Route>
      <Route path="/new_passwd" exact component={NewPassword}></Route>
      <Route path="/victimizer_manage" exact component={Victimizer}></Route>
      <Route path="/request_management/request_map" exact component={RequestMap}></Route>
      <Route path="/request_management" exact component={RequestHelp}></Route>
      <Route path="/request_management/detail/:id" exact component={RequestHelpDetail}></Route>
      <Route path="/test_intra_family_violence" exact component={TestIntraFamilyViolence}></Route>
    </Router>
  );
}

export default App;